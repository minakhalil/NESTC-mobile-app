<?php

namespace App\Libraries;


class CustomResponse{
    

    public function __construct($error = false, $message = null, $returnedObject = null,$specialErrorStatus=null) {
        //print "In constructor\n";
        $this->error = $error;
        $this->message = $message;
        $this->returnedObject = $returnedObject;
        $this->specialErrorStatus = $specialErrorStatus;
    }

    public function getJson(){
        $json = json_encode(array(
        "error" => $this->error,
        "message" => $this->message,
        "returnedObject" => $this->returnedObject,
        ), JSON_UNESCAPED_UNICODE);

        return $json;
    }

    public function getResponse(){
        
        if($this->specialErrorStatus){
            return \Response::make($this->getJson(), $this->specialErrorStatus);
        }
        else
        {
            return response($this->getJson());
        }

        //return response($this->getJson())->setStatusCode($this->specialErrorStatus);
    }


}