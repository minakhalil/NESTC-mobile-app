<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\PushNotification;
use Illuminate\Http\Request;
use App\Libraries\CustomResponse as CustomResponse;
use GuzzleHttp\Client;
class EventController extends Controller
{
    
     public function getGuestsList(Event $event)
    {
        //
        
        return view('registeredGuests.index',[
            'event' => Event::find($event->id),
            'guests' => Event::find($event->id)->guests()->get(),
   
        ]);
    }
     /// it's for web
       public function getGeoLocations(Event $event)
    {
        //
        $locations = Event::find($event->id)->locations()->get();
        foreach($locations as $location){
            $location->details = json_decode($location->jsonDetails);
        }
        return view('geoLocations.index',[
            'event' => Event::find($event->id),
            'locations' => $locations,
   
        ]);
    }
    /// it's for api
           public function getUpdatedLocations(Event $event)
    {
        //
        $locations = Event::find($event->id)->locations()->get();
      
        return $locations;
    }

    public function getEventForIntro(){
        return Event::latest()->with('sponsors')->get();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            return view('events.index',[
            'events' => Event::where('isActive',1)->get(),
            
   
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $toSave = $request->only([
            'title',
            'description',
            'startDate',
            'endDate',
            'isActive',
            'imgUrl',
            'localMapUrl',
            'instVideo',
            'centerLat',
            'centerLng',
            'radius',
            'zoomLevel',
            'helpNumber',
            'helpEmail',
            'helpText',
            'jsonDetails',
            ]);
        $event = Event::create($toSave);

       
        /*
        * Onesignal api call
        *
        * //subscribe to dummy device
        *
        */

           $dummyHeaders = ['Content-Type' => 'application/json; charset=utf-8'];
            $dummyBody = json_encode(array(
                "app_id" => "7303a016-ad64-4cdc-ae18-0476618ee168",
                "identifier" => "ce777617da7bbnf548fe7a9ab6febb56fgdrd",
                "device_type" => "1",
                "tags" => array("event".$event->id => true)                 
            ));


        $dummyClient = new Client(['headers' => $dummyHeaders]);
        $dummyApiResponse = $dummyClient->request('POST', 'https://onesignal.com/api/v1/players', ['body' => $dummyBody]);

        PushNotification::sendScheduleNotification($event->id);

        $response  = new CustomResponse(false,'Event Added Sucessfully',null);
        return $response->getJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
    }
}
