<?php

namespace App\Http\Controllers;

use App\Models\GeoLocation;
use App\Models\Event;
use Illuminate\Http\Request;
use App\Libraries\CustomResponse as CustomResponse;

class GeoLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $toSave = $request->only([
            'title',
            'description',
            'icon',
            'jsonDetails',
            'centerLat',
            'centerLng',
            'radius',            
            'event_id'
            ]);
        $geoLocation = GeoLocation::create($toSave);
        $response  = new CustomResponse(false,'GeoLocation Added Sucessfully',null);
        return $response->getJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GeoLocation  $geoLocation
     * @return \Illuminate\Http\Response
     */
    public function show(GeoLocation $geoLocation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GeoLocation  $geoLocation
     * @return \Illuminate\Http\Response
     */
    public function edit(GeoLocation $geoLocation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GeoLocation  $geoLocation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GeoLocation $geoLocation)
    {
        
        if($request->ajax()){
        $location = GeoLocation::find($geoLocation->id);
        $locationDetails = json_decode($location->jsonDetails);
        if(isset($request['currentCapacity']))
        $locationDetails->currentCapacity = $request['currentCapacity'];
        if(isset($request['isOpen']))
        $locationDetails->isOpen = $request['isOpen'];
        $location->jsonDetails = json_encode($locationDetails);
        $location->update();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GeoLocation  $geoLocation
     * @return \Illuminate\Http\Response
     */
    public function destroy(GeoLocation $geoLocation)
    {
        //
    }
}
