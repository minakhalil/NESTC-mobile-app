<?php

namespace App\Http\Controllers;

use App\Models\RegisteredGuest;
use Illuminate\Http\Request;
use App\Libraries\CustomResponse as CustomResponse;
use Mail;

class RegisteredGuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $toSave = json_decode($request->getContent(), true);
        $prevRegisterd = RegisteredGuest::where('email',$toSave['email']);

        if($prevRegisterd->count()>0&&isset($toSave['email'])&&!is_null($toSave['email'])&&$toSave['email']!="")
        {
           $registeredGuest = $prevRegisterd->first();
           /*
           *    HERE DUPLICATE GUEST MAIL VERIFICATION
           *
           */
           // Generate a key of length 4 digits;

        //    $key = rand(pow(10, 3), pow(10, 4)-1);

        //    GuestToken::create([
        //          'registeredGuest_id' => $registeredGuest->name,
        //          'key' => $key,
        //             ]);

        //     Mail::send('emailView', ['user' => $registeredGuest,'key'=>$key], function ($m) use ($user) {
        //     $m->from('info@titrias.com', 'NESTC');
        //     $m->to($registeredGuest->email, $registeredGuest->name)->subject('Make verification for your email');
        //     });

        //    $response  = new CustomResponse(true,'Duplicate email',null);


        }
        else
        $registeredGuest = RegisteredGuest::create($toSave);
        return $registeredGuest;
    }




    public function makeVerification(Request $request)
    {
        $toSave = json_decode($request->getContent(), true);
       
           $guest = RegisteredGuest::where('email',$toSave['email'])->first();
           // should compare with reated at to make time expiration
           $originalToken = GuestToken::where('registeredGuest_id',$guest->id)->where('done',1)->first();
           if($originalToken->key == $toSave['key']){
               // true
               $originalToken->done=0;
               $originalToken->update();
               $response  = new CustomResponse(false,'Done',$guest);
           }else{
               $response  = new CustomResponse(true,'Error with token',null);
           }

      
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RegisteredGuest  $registeredGuest
     * @return \Illuminate\Http\Response
     */
    public function show(RegisteredGuest $registeredGuest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RegisteredGuest  $registeredGuest
     * @return \Illuminate\Http\Response
     */
    public function edit(RegisteredGuest $registeredGuest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RegisteredGuest  $registeredGuest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegisteredGuest $registeredGuest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RegisteredGuest  $registeredGuest
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegisteredGuest $registeredGuest)
    {
        //
    }
}
