<?php

namespace App\Http\Controllers;

use App\Models\Info;
use Illuminate\Http\Request;
use App\Libraries\CustomResponse as CustomResponse;

class InfoController extends Controller
{
    /**
    VIEW should be implemented
    UPDATE **MIGHT** be implemented
    **/
    public function getInfo(Request $request){
        $info = Info::get()[0];
        $response = new CustomResponse(false,'',$info);
        return $response->getResponse();
    }
}
