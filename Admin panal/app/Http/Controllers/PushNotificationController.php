<?php

namespace App\Http\Controllers;

use App\Models\PushNotification;
use App\Models\Event;
use App\Models\NotificationTemplate;
use App\Models\RegisteredGuest;
use Illuminate\Http\Request;
use App\Libraries\CustomResponse as CustomResponse;
use GuzzleHttp\Client;
use Session;
use Redirect;
use DateTime;
use DateInterval;

class PushNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('pushNotifications.index',[

            'pushNotifications' => PushNotification::get(),
   
        ]);
    }

     public function getPush(RegisteredGuest $user = null)
    {

    $notifications=PushNotification::whereIn('event_id',!is_null($user)?$user->events->pluck('id'):[])
                                    ->WhereIn('user_id', !is_null($user)?[$user->id]:[],'or')
                                    ->orWhere('event_id', null)
                                    ->where('user_id', null,'and')
                                    ->latest()
                                    ->with('event')
                                    ->get();

        return $notifications ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     
    public function create()
    {
        $events = Event::pluck('title', 'id');
        $events[""]= "الكل";
        return view('pushNotifications.create',[

            'events' => $events,
   
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function sendScheduleNotification($eventID){

        $templates = NotificationTemplate::get();
        $event= Event::find($eventID);
        
        foreach($templates as $template)
        {

        $text = $template->text;
        $query = $template->query;


        $text = str_replace("[name]",$event->title,$text);

        
        $intervalTo = preg_replace('/\[.*\]/', '', $query);


        if (strpos($query, '[start]')!== false)
            $sendTime = $event->startDate;
        else
        if(strpos($query, '[end]')!== false)
            $sendTime =  $event->endDate;



        $dt = new DateTime($sendTime);

        if(strpos($query, '[+]')!== false)
            $dt->add(new DateInterval('PT'.$intervalTo.'H'));

        if(strpos($query, '[-]')!== false)
            $dt->sub(new DateInterval('PT'.$intervalTo.'H'));    

         $dt = $dt->format('Y-m-d H:i:s e');

        

        $headers = ['Content-Type' => 'application/json; charset=utf-8','Authorization'=> 'Basic M2E1MWYxZjktNTk0YS00NzgyLTlhZWYtNDc2NDU0Y2MxMDlk'];
        $appId = "7303a016-ad64-4cdc-ae18-0476618ee168";
        $content = array(
            "en" => $text
            );
        $heading = array(
            "en" => "NESTC"
            );

        $body = json_encode(array(
            "app_id" => $appId,
            "included_segments" => ["All"],
            "send_after" => $dt,
            "contents" => $content,
            "headings" => $heading
        ));

        $client = new Client(['headers' => $headers]);
        $apiResponse = $client->request('POST', 'https://onesignal.com/api/v1/notifications', ['body' => $body]);
        
        $pushNotifiCation = PushNotifiCation::create([
            "type"=>"group",
            "title"=>"NESTC",
            "description"=>$text
        ]);
    
    }

     }
    public function store(Request $request)
    {
        if($request['type']=="user"){
          $this->validate($request, [
        'title' => 'required',
        'description' => 'required',
        'type' => 'required',
        'user_id'=>'required'
             ]);

             $guest = RegisteredGuest::find($request['user_id']);
             if(is_null($guest))
             return Redirect::back()->withErrors(['كود الزائر غير صحيح'])->withInput();
             $guest =  $guest->tracking_id;
        }
        else
            $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'type' => 'required',
                ]);


        

        $toSave = $request->only([
            'title',
            'description',
            'event_id',
            'user_id',
            'type'
            ]);
            ///
       //dd($toSave);
         
        /*
        * SEND TO ONESIGNAL API
        */

        // Apply Tags to get for a specific event
        
        $headers = ['Content-Type' => 'application/json; charset=utf-8','Authorization'=> 'Basic M2E1MWYxZjktNTk0YS00NzgyLTlhZWYtNDc2NDU0Y2MxMDlk'];
        $appId = "7303a016-ad64-4cdc-ae18-0476618ee168";
        $content = array(
            "en" => $request['description']
            );
        $heading = array(
            "en" => $request['title']
            );



            if($request['type']=="user"){

                // /// SEND TO SPECIFIC GUEST

            $toSave['event_id']=null;
            $body = json_encode(array(
            "app_id" => $appId,
            "include_player_ids" => [$guest],
            "contents" => $content,
            "headings" => $heading
            ));

            }else{
                $toSave['user_id']=null;
        if(isset($request['event_id'])&&is_null($request['event_id'])){
            
            // /// SEND TO ALL

            $body = json_encode(array(
            "app_id" => $appId,
            "included_segments" => ["All"],
            "contents" => $content,
            "headings" => $heading
            ));

        }else{

            /// SEND TO GROUP
            
            $tags = array(
                "key"=>"event".$request['event_id'],
                "relation"=>"=",
                "value"=>true
            );

            $body = json_encode(array(
            "app_id" => $appId,
            "tags" => [$tags],
            "contents" => $content,
            "headings" => $heading
            ));
                
        }

            }
        

      
     

        
        
          $client = new Client(['headers' => $headers]);
          $apiResponse = $client->request('POST', 'https://onesignal.com/api/v1/notifications', ['body' => $body]);
        
        // to save onesignal notification id $apiResponse->id

        $pushNotifiCation = PushNotifiCation::create($toSave);

        //Session::flash('flash_message_success', 'تم الارسال بنجاح');

         return redirect()->route('pushNotifications.index');
    }



}
