<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\CustomResponse as CustomResponse;
use App\Models\Event;
use App\Models\Info;
use App\Models\RegisteredGuest;
use Carbon\Carbon;
class MultipleEntitiesController extends Controller
{
    // public static function vincentyGreatCircleDistance(
    // $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    // {
    // // convert from degrees to radians
    // $latFrom = deg2rad($latitudeFrom);
    // $lonFrom = deg2rad($longitudeFrom);
    // $latTo = deg2rad($latitudeTo);
    // $lonTo = deg2rad($longitudeTo);

    // $lonDelta = $lonTo - $lonFrom;
    // $a = pow(cos($latTo) * sin($lonDelta), 2) +
    //     pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
    // $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

    // $angle = atan2(sqrt($a), $b);
    // return $angle * $earthRadius;
    // }

    private static function isClose($clientLng,$clientLat,$eventLng,$eventLat,$radius)
    {
      //return false;
      if(is_null($clientLat)&&is_null($clientLng)) return true;

        $distance = (6371 * acos(cos(deg2rad($clientLat)) 
                     * cos(deg2rad($eventLat)) 
                     * cos(deg2rad($eventLng) 
                     - deg2rad($clientLng)) 
                     + sin(deg2rad($clientLat)) 
                     * sin(deg2rad($eventLat))));
        //dd($distance);
        
        if($distance < ($radius+0.1)){
            return true;
        }
    
    }
    public function getCompleteData(Request $request)
    {
     
        $info = Info::get()[0];
        $toSave = json_decode($request->getContent(), true);

        
        $userID = $toSave['userId'];
        $lat = $toSave['lat'];
        $lng = $toSave['lng'];

        $currentUser = RegisteredGuest::find($userID);
 
        $event = Event::whereDate('startDate','<=',Carbon::today()->toDateString())->whereDate('endDate','>=',Carbon::today()->toDateString())->where('isActive','=',1)->with('geoLocations');
        
        $closeEvent = null;
        $invetationData=null;

        $events = $event->get();
        
        
        for($eIndex=0;$eIndex<count($events);$eIndex++)
        {
            $currentEvent = $events[$eIndex];
            if(MultipleEntitiesController::isClose($lng,$lat,$currentEvent->centerLng,$currentEvent->centerLat,$currentEvent->radius)){
                $closeEvent = $currentEvent;
                $invetationData =  $currentUser->events()->sync($closeEvent);
                break;
            }
        }
       
       
       $invetationData = (isset($invetationData)&&isset($invetationData["attached"][0])&&!is_null($invetationData))?$invetationData["attached"][0]:null;

        $obj = (object)array(
            'event' => $closeEvent,
            'info' => $info,
            'invitation'=>$invetationData
        );



        //$response = new CustomResponse(null,"",$obj);
        return json_encode(["data"=>$obj]);

    }
}
