<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Info extends Model
{
    use SoftDeletes;
    public $table = "info";
    protected $dates = ['deleted_at'];
    protected $guarded =[]; // all fields are fillable

}
