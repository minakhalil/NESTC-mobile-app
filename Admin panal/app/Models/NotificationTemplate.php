<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotificationTemplate extends Model
{
    use SoftDeletes;
    public $table = "notification_templates";
    
    protected $dates = ['deleted_at'];
    protected $guarded =[]; // all fields are fillable
 
    
}
