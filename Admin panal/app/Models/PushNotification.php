<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PushNotification extends Model
{
    use SoftDeletes;
    
    public $table = "pushNotifications";

    protected $dates = ['deleted_at'];
    protected $guarded =[]; // all fields are fillable

    public function event(){
        return $this->belongsTo('App\Models\Event')->select(array('id', 'title'));
    }

    public function user(){
        return $this->belongsTo('App\Models\RegisteredGuest')->select(array('id'));
    }

    
}
