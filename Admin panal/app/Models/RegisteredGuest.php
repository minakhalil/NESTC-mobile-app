<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Event;

class RegisteredGuest extends Model
{
    use SoftDeletes;
    public $table = "registeredGuests";
    protected $dates = ['deleted_at'];
    protected $guarded =[]; // all fields are fillable
    
      public function events()
    {
      return $this->belongsToMany(Event::class,'registeredGuest_event','registeredGuest_id','event_id')->withTrashed();
    }
}
