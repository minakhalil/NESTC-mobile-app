<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sponsor extends Model
{
    use SoftDeletes;
    public $table = "sponsors";
    
    protected $dates = ['deleted_at'];
    protected $guarded =[]; // all fields are fillable
    public function event(){
        return $this->belongsTo('App\Models\Event');
    }
    
}
