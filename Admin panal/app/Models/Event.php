<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\RegisteredGuest;
class Event extends Model
{
    use SoftDeletes;
    public $table = "events";
    
    protected $dates = ['deleted_at'];
    protected $guarded =[]; // all fields are fillable
    
    public function registeredGuests()
    {
        return $this->hasMany('App\Models\RegisteredGuest');
    }

    public function geoLocations()
    {
        return $this->hasMany('App\Models\GeoLocation');
    }
   
    public function pushNotifications()
    {
        return $this->hasMany('App\Models\PushNotification');
    }

    public function locations()
    {
        return $this->hasMany('App\Models\GeoLocation');
    }

    public function sponsors()
    {
        return $this->hasMany('App\Models\Sponsor')->orderBy('index', 'asc');
    }

        public function guests()
    {
      return $this->belongsToMany(RegisteredGuest::class,'registeredGuest_event','event_id','registeredGuest_id')->withTrashed();
    }
}
