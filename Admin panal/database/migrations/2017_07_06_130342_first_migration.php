<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FirstMigration extends Migration
{

    private function addEventsTable(){
        Schema::create('events', function (Blueprint $table) {
         $table->increments('id');
         $table->string('title');
         $table->text('description');
         $table->dateTime('startDate')->nullable();
         $table->dateTime('endDate')->nullable();
         $table->boolean('isActive'); // Some  events may be cancelled after being started.
         $table->string('imgUrl');
         $table->string('localMapUrl')->nullable();
         $table->string('instVideo')->nullable();
         $table->double('centerLat')->nullable();
         $table->double('centerLng')->nullable();
         $table->double('radius')->nullable();
         
         $table->integer('zoomLevel')->nullable();
         $table->text('helpNumber')->nullable();
         
         $table->string('helpEmail')->nullable();
         $table->text('helpText')->nullable();
         $table->text('jsonDetails')->nullable();

         $table->timestamps();
          //$this->addByCols($table);
         $table->softDeletes();

       });
    }
    private function addRegisteredTable(){
        Schema::create('registeredGuests', function (Blueprint $table) {
         $table->increments('id');
         $table->string('name')->nullable();
         //$table->string('mobile')->nullable();
         $table->string('phone')->nullable();
         $table->string('email')->nullable();
         $table->text('address')->nullable(); 
         $table->boolean('isMale')->nullable();
         $table->string('car')->nullable();
         $table->string('tracking_id')->nullable();
         $table->timestamps();
          //$this->addByCols($table);
         $table->softDeletes();

       });

        Schema::create('registeredGuest_event', function (Blueprint $table) {
         $table->increments('id');
                
         $table->timestamps();
         
         $table->softDeletes();
         $table->integer('event_id')->unsigned();
         $table->foreign('event_id')->references('id')->on('events');

         $table->integer('registeredGuest_id')->unsigned();
         $table->foreign('registeredGuest_id')->references('id')->on('registeredGuests');

       });



        Schema::create('guestsTokens', function (Blueprint $table) {
         $table->increments('id');
                
         
         $table->integer('key');
         $table->boolean('done')->default(0);
        

         $table->integer('registeredGuest_id')->unsigned();
         $table->foreign('registeredGuest_id')->references('id')->on('registeredGuests');

        $table->timestamps();
        $table->softDeletes();

       });


    }    
    private function addPushNotificationsTable(){
        Schema::create('pushNotifications', function (Blueprint $table) {
         $table->increments('id');
         $table->string('type');
         $table->string('title');
         $table->text('description');
         
         $table->timestamps();
          //$this->addByCols($table);
         $table->softDeletes();

         $table->integer('event_id')->unsigned()->nullable();
         $table->foreign('event_id')->references('id')->on('events');

        $table->integer('user_id')->unsigned()->nullable();
         $table->foreign('user_id')->references('id')->on('registeredGuests');
       });



        Schema::create('sponsors', function (Blueprint $table) {
         
         $table->increments('id');
         $table->string('name');
         $table->text('imgUrl')->nullable();

         $table->integer('index')->nullable();
         $table->string('type')->nullable();
         $table->string('link')->nullable();
         $table->timestamps();
          //$this->addByCols($table);
         $table->softDeletes();

         $table->integer('event_id')->unsigned()->nullable();
         $table->foreign('event_id')->references('id')->on('events');

       });


          Schema::create('notification_templates', function (Blueprint $table) {
         
         $table->increments('id');
         $table->text('text')->nullable();
         $table->text('description')->nullable();
         $table->string('query')->nullable();
   
       
       
         $table->timestamps();
         $table->softDeletes();


       });

    }       
    private function addInfoTable(){
        Schema::create('info', function (Blueprint $table) {
         //$table->increments('id');
         $table->text('details');
         $table->text('fbLink');
         $table->timestamps();
          //$this->addByCols($table);
         $table->softDeletes();

       });
    }
    private function addGeoLocationTable(){
        Schema::create('geoLocations', function (Blueprint $table) {
         $table->increments('id');
         $table->string('title');
         $table->text('description');
         
         $table->integer('type'); // 0-> marker // 1->polygon
         // These three parameters are used for getting the distance between the user and the geoLocation, if needed someday
         $table->double('centerLat')->nullable();
         $table->double('centerLng')->nullable();
         $table->double('radius')->nullable();
         $table->text('style')->nullable(); // if marker-> bas64 icon image // if polygon -> rgba color
         $table->text('jsonDetails');
         $table->text('indoor')->nullable();
         /*
         Json should contain the type and the related parameters.

         */
         $table->timestamps();
          //$this->addByCols($table);
         $table->softDeletes();
         $table->integer('event_id')->unsigned();
         $table->foreign('event_id')->references('id')->on('events');

       });
    }
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $this->addEventsTable();
        $this->addRegisteredTable();
        $this->addPushNotificationsTable();
        $this->addInfoTable();
        $this->addGeoLocationTable();
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('registeredGuest_event');
        Schema::drop('geoLocations');
        Schema::drop('sponsors');
        Schema::drop('info');
        Schema::drop('notification_templates');
        Schema::drop('pushNotifications');
        Schema::drop('registeredGuests');
        Schema::drop('events');
        
    }
}
