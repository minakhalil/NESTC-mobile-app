<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('notification_templates')->insert([
            'text'=>'تبدأ غداً فعاليات [name]',
            'description'=>'ترسل قبل المعرض بيوم',
            'query'=>'[start][-]24',
            ]);

        DB::table('notification_templates')->insert([
            'text'=>'تبدأ اليوم فعاليات [name]',
            'description'=>'ترسل يوم المعرض في الصباح',
            'query'=>'[start][+]0',
            ]);

        DB::table('notification_templates')->insert([
            'text'=>'ينتهي في خلال ساعات [name]',
            'description'=>'ترسل قبل نهاية المعرض بساعتين',
            'query'=>'[end][-]2',
            ]);

        DB::table('users')->insert([
            'name'=>'admin',
            'email'=>'admin@titrias.com',
            'password'=>bcrypt('123456')
            ]);

        DB::table('info')->insert(['details'=>'dhsjhkjhhgjgkdhshdg','fbLink'=>'http://www.facebook.com']);

        DB::table('events')->insert([
            'title'=>'معرض الكتاب',
            'description'=>'وصف خاص لمعرض الكتاااب',
            'isActive'=>'1',
            "centerLat"=>"30.065678",
            "centerLng"=>"31.307731",
            "zoomLevel"=>"15",
            "helpNumber"=> '[{"title":"طوارئ","number":"+123513235"},{"title":"امن","number":"+22222"}]',
            "helpEmail"=> "admin@titrias.com",
            'radius'=>"5000",
            "startDate"=>"2017-08-01 00:00:00",
            "endDate"=>"2017-09-01 00:00:00",
            "imgUrl"=>"http://via.placeholder.com/120x120",
            "localMapUrl"=>"http://via.placeholder.com/1000x1000",
            "instVideo"=>"https://www.youtube.com/embed/P5zs-1MrBhM"
            
            ]);

        // DB::table('geoLocations')->insert([
        //     'title'=>'ماركر 1',
        //     'description'=>'وصف لماركر 1',
        //     "centerLat"=>"29.846238",
        //     "centerLng"=>"31.329054",
        //     "jsonDetails"=>'{"currentCapacity":"18","totalCapacity":"50"}',
        //     'event_id'=>"1",
        //     "type"=>"0"
        //     ]);

        //     DB::table('geoLocations')->insert([
        //     'title'=>'ماركر 2',
        //     'description'=>'2',
        //     "centerLat"=>"29.849549",
        //     "centerLng"=>"31.328877",
        //     "jsonDetails"=>'{"isOpen":"1"}',
        //     'event_id'=>"1",
        //     "type"=>"0"
        //     ]);

        //     DB::table('geoLocations')->insert([
        //     'title'=>'ماركر 3',
        //     'description'=>'وصف لماركر 3',
        //     "centerLat"=>"29.854034",
        //     "centerLng"=>"31.329934",
        //     "jsonDetails"=>'{"emergency":"1"}',
        //     'event_id'=>"1",
        //     "type"=>"0"
        //     ]);

        //     DB::table('geoLocations')->insert([
        //     'title'=>'بولي 3',
        //     'description'=>'وصف لبولي 3',
        //     "jsonDetails"=>'{"currentCapacity":"0","totalCapacity":"50","points": [{"lat": 29.846238,"lng": 31.329054},{"lat": 29.849549,"lng": 31.328877},{"lat": 29.854034,"lng": 31.329934}]}',
        //     'event_id'=>"1",
        //     "type"=>"1",
        //     "indoor"=>'["محل 1","محل 2","محل 3"]'
        //     ]);


            DB::table('geoLocations')->insert([
            'title'=>'ساحة انتظار',
            'description'=>'',
            "jsonDetails"=>'{"currentCapacity":"0","totalCapacity":"50","points": [ {"lat":30.066397, "lng":31.306147}, {"lat":30.066757, "lng":31.307257}, {"lat":30.066007, "lng":31.307653}, {"lat":30.065768, "lng":31.307592}, {"lat":30.065353, "lng":31.307704}, {"lat":30.065011, "lng":31.306742} ]}',
            'event_id'=>"1",
            "centerLat"=>"29.854034",
            "centerLng"=>"31.329934",
            "type"=>"1",
            "style"=>"rgba(215, 255, 44, 0.6)",
            ]);

            DB::table('geoLocations')->insert([
            'title'=>'قاعة 3',
            'description'=>'',
            "jsonDetails"=>'{"points": [ { "lat":30.065664, "lng":31.308508 }, { "lat":30.065845, "lng":31.308466 }, { "lat":30.065917, "lng":31.308526 }, { "lat":30.065980, "lng":31.309007 }, { "lat":30.065513, "lng":31.309125 }, { "lat":30.065464, "lng":31.308843 } ]}',
            'event_id'=>"1",
            "type"=>"1",
            "centerLat"=>"30.065739",
            "centerLng"=>"31.308784",
            "style"=>"rgba(40, 255, 240, 0.6)",
            "indoor"=>'["محل 1","محل 2","محل 3"]'
            ]);

            DB::table('geoLocations')->insert([
            'title'=>'قاعة 2',
            'description'=>'',
            "jsonDetails"=>'{"points": [ { "lat":30.065664, "lng":31.308508 }, { "lat":30.065464, "lng":31.308843 }, { "lat":30.065419, "lng":31.308810 }, { "lat":30.065350, "lng":31.308919 }, { "lat":30.065054, "lng":31.308691 }, { "lat":30.065112, "lng":31.308567 }, { "lat":30.065068, "lng":31.308531 }, { "lat":30.065253, "lng":31.308193 } ]}',
            'event_id'=>"1",
            "type"=>"1",
            "centerLat"=>"30.065298",
            "centerLng"=>"31.308622",
            "style"=>"rgba(255, 40, 44, 0.6)",
            "indoor"=>'["محل 1","محل 2","محل 3"]'
            ]);

            DB::table('geoLocations')->insert([
            'title'=>'قاعة 1',
            'description'=>'',
            "jsonDetails"=>'{"points": [ { "lat":30.065253, "lng":31.308193 }, { "lat":30.065239, "lng":31.307979 }, { "lat":30.065137, "lng":31.307904 }, { "lat":30.064719, "lng":31.307991 }, { "lat":30.064826, "lng":31.308588 }, { "lat":30.065068, "lng":31.308531 } ]}',
            'event_id'=>"1",
            "type"=>"1",
            "centerLat"=>"30.064997",
            "centerLng"=>"31.308227",
            "style"=>"rgba(40, 255, 240, 0.6)",
            "indoor"=>'["محل 1","محل 2","محل 3"]'
            ]);


    }
}
