            @extends('layouts.master')

@section('content')
<a href="{{ url('/pushNotifications/create') }}"  class="btn btn-success">اشعار جديد</a>
<br />
            @if (count($pushNotifications) > 0)
                <div class="panel panel-default" style="margin-top:10px;">
                
                    <div class="panel-heading">
                      الاشعارات

                    </div>

                    <div class="panel-body">
                        <table class="table table-striped task-table" id="mainTable">
                            <thead>
                                <th>العنوان</th>
                                <th>الوصف</th>
                                <th>تم الارسال الي</th>
                            </thead>
                            <tbody>
                                @foreach ($pushNotifications as $push)
                                    <tr>
                                        <td class="table-text"><div>{{ $push->title }}</div></td>
                                        <td >{{ $push->description }}</td>
                                        <td >
                                        @if (is_null($push->event_id)&&is_null($push->user_id))
                                        <div class="chip bg-primary">الكل</div>
                                        @elseif(!is_null($push->event_id)&&is_null($push->user_id))
                                        <div class="chip bg-success">{{ $push->event->title }}</div>
                                        @else
                                        <div class="chip bg-danger">
                                            مخصصة ({{ $push->user->id }})
                                        </div>
                                        @endif
                                        </td >
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <script>
                    $(document).ready(function() {
                    sessionStorage.removeItem('type');
                    });
                    </script>
                </div>
            @endif
            @stop