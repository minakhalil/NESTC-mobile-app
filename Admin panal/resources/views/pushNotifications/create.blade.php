@extends('layouts.master')

@section('content')
<a href="{{ url('/pushNotifications/') }}"  class="btn btn-danger">عودة الي الاشعارات</a>
<h1>ارسال اشعار جديد</h1>

<hr>
@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
@if(Session::has('flash_message_success'))
    <div class="alert alert-success">
        {{ Session::get('flash_message_success') }}
    </div>
@endif
{!! Form::open(['route' => 'pushNotifications.store']) !!}
<style>
.segmented-control input[type="radio"] {
    display: none;
}
.segmented-control{
    text-align:center;
}
.segmented-control .list-group-item {
    display: inline-block;
    text-align:center;
    width:200px;
    
}
.segmented-control .list-group-item {
	margin-bottom: 0;
	margin-left:-4px;
	margin-right: 0;
}
.segmented-control .list-group-item:last-child {
	border-top-left-radius:20px;
	border-bottom-left-radius:20px;
}
.segmented-control .list-group-item:first-child {
	border-top-right-radius:20px;
	border-bottom-right-radius:20px;
}

</style>
<div class="form-group">
    {!! Form::label('title', 'العنوان:', ['class' => 'control-label']) !!}
    {!! Form::text('title', 'NESTC', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('description', 'الوصف:', ['class' => 'control-label']) !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>


 <div class="form-group">
<div class="list-group segmented-control">

        @if(!is_null(app('request')->input('to')))
                <a href="#" id="group" class="list-group-item ">
                    مجموعة
                    <input type="radio"   name="type" value="group"/>
                </a>
                <a href="#" id="user" class="list-group-item active">
                    شخص
                    <input type="radio"  checked name="type" value="user"/>
                </a>
            </div>
            @else

              <a href="#" id="group" class="list-group-item active">
                    مجموعة
                    <input type="radio"  checked name="type" value="group"/>
                </a>
                <a href="#" id="user" class="list-group-item ">
                    شخص
                    <input type="radio"  name="type" value="user"/>
                </a>
            </div>
            @endif

  </div>


 <div class="form-group hidden">
 {!! Form::label('event','كود الزائر:',null, ['class' => 'control-label']) !!}
 {!! Form::text('user_id', app('request')->input('to') , ['class' => 'form-control']) !!}
  </div>


 <div class="form-group hidden">
 {!! Form::label('event','ارسال الي:',null, ['class' => 'control-label']) !!}
 {!! Form::select('event_id',
 $events,
 null,
 ['class' => 'form-control']) !!}
  </div>


<p style="color:red"> يرجي التأكد جيدا من البيانات قبل ارسالها حيث ليس هناك مجال تعديلها</p>

{!! Form::submit('ارسال', ['class' => 'btn btn-primary']) !!}

{!! Form::close() !!}

<script>    
  $(document).ready(function() {

      var selected = sessionStorage.getItem('type');
      if(selected){
        console.log(selected);

        $('div.segmented-control a').each(function(i,e){
            $(e).removeClass('active');
        });

        $('#'+selected).addClass('active');
        $('#'+selected).find('input').prop('checked',true);

      }

   if($('div.segmented-control a.active').find('input').val()=="group"){

            $('form select[name="event_id"]').parent().removeClass('hidden');
            $('form input[name="user_id"]').parent().addClass('hidden');
        }
        if($('div.segmented-control a.active').find('input').val()=="user"){

            $('form input[name="user_id"]').parent().removeClass('hidden');
            $('form select[name="event_id"]').parent().addClass('hidden');
        }


    $('div.segmented-control a').on('click', function(){
        
        

        $('div.segmented-control a').each(function(i,e){
            $(e).removeClass('active');
        });
        
        $(this).addClass('active');
        $(this).find('input').prop('checked',true);
        sessionStorage.setItem('type', $(this).find('input').val());
        if($(this).find('input').val()=="group"){

            $('form select[name="event_id"]').parent().removeClass('hidden');
            $('form input[name="user_id"]').parent().addClass('hidden');
        }
        if($(this).find('input').val()=="user"){

            $('form input[name="user_id"]').parent().removeClass('hidden');
            $('form select[name="event_id"]').parent().addClass('hidden');
        }
        return false;
        
    });

    });
    


</script>
@stop