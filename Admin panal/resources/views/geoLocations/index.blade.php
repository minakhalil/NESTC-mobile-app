@extends('layouts.master')

@section('content')
            @if (count($locations) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                      العلامات

                    </div>

                    <div class="panel-body">
                        <table class="table table-striped task-table" id="mainTable">
                            <thead>
                                <th>العنوان</th>
                                <th>الوصف</th>
                                <th>عمليات</th>
                            </thead>
                            <tbody>
                                @foreach ($locations as $location)
                                    @if(isset($location->details->currentCapacity)&&!is_null($location->details->currentCapacity))
                                    <tr>
                                        <td class="table-text"><div>{{ $location->title }}</div></td>
                                        <td >{{ $location->description }}</td>
                                        <td class="control-group error">
                                        <span id="capacity{{ $location->id }}">{{ $location->details->totalCapacity }}</span>/
                                        <input type="number" id="input{{ $location->id }}" value="{{ $location->details->currentCapacity }}"  style="height:43px;width:80px" type="text" >
                                        <button type="button" id="btn{{ $location->id }}" class="btn btn-info submitBtn">
                                        <i class="fa fa-check fa-2x"></i>
                                        </button>
                                        <p id="errorMsg{{ $location->id }}" style="font-size:7x;color:red;display:none">*خطأ</p>
                                        </td>
                                    </tr>
                                    @endif
                                    @if(isset($location->details->isOpen)&&!is_null($location->details->isOpen))
                                    <tr>
                                        <td class="table-text"><div>{{ $location->title }}</div></td>
                                        <td >{{ $location->description }}</td>
                                        <td class="control-group error">
                                        <select  id="select{{ $location->id }}" style="height:43px;width:115px">
                                            <option value="1" {{($location->details->isOpen==1)?"selected":""}} >مفتوحة</option>
                                            <option value="0" {{($location->details->isOpen==0)?"selected":""}}>مغلقة</option>
                                        </select>
                                        <button type="button" id="btn{{ $location->id }}" class="btn btn-info gateBtn">
                                        <i class="fa fa-check fa-2x"></i>
                                        </button>
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <meta name="_token" content="{!! csrf_token() !!}" />
                    <script>

                $(document).ready(function() {
                   
                    $(".submitBtn").click(function(){

                   var element = $(this).attr('id');
                   var elementID = element.substr(3,element.length);

                   
                   if((!(parseInt($("#capacity"+elementID).html())>=parseInt($("#input"+elementID).val())))||parseInt($("#input"+elementID).val()<0))
                    {
                       
                        $("#errorMsg"+elementID).css("display", "block");
                    }
                    else{
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        })
                        $.ajax({

                        type: "PUT",
                        url: '/geoLocations/'+elementID,
                        data: {currentCapacity: parseInt($("#input"+elementID).val())},
                        success: function( msg ) {
                        $("#errorMsg"+elementID).css("display", "none");
                        }
                    });

                        
                    }
                    
                    
                    });



                    $(".gateBtn").click(function(){

                   var element = $(this).attr('id');
                   var elementID = element.substr(3,element.length);

                   
                
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        })
                        $.ajax({

                        type: "PUT",
                        url: '/geoLocations/'+elementID,
                        data: {isOpen: parseInt($("#select"+elementID).val())},
                        success: function( msg ) {
                        
                        }
                    });

                        
                    
                    
                    
                    });
                    
                });
                    </script>
                </div>
            @endif
            @stop