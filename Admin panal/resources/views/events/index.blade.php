            @extends('layouts.master')

@section('content')
            @if (count($events) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                    الاحداث المفعلة
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped task-table" id="mainTable">
                            <thead>
                                <th>الحدث</th>
                                <th>عمليات</th>
                                
                            </thead>
                            <tbody>
                                @foreach ($events as $event)
                                    <tr>
                                      
                                        <td class="table-text"><div>{{ $event->title }}</div></td>
                                        <td>
                                        <a title="ساحات الانتظار" href="{{ url('/events/'.$event->id.'/geoLocations') }}" class="btn btn-primary btn-circle"><i class="fa fa-map-marker fa-3x"></i></a>
                                        <a title="السادة الزوار" href="{{ url('/events/'.$event->id.'/guests') }}" class="btn btn-danger btn-circle"><i class="fa fa-user fa-3x"></i></a>
                                        </td>
                                        
                                       
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
            @stop