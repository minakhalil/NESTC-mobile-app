            @extends('layouts.master')

@section('content')
            @if (count($guests) > 0)
                <div class="panel panel-default">
    <style>
    #searchclear {
    position: absolute;
    left: 25px;
    top: 0;
    bottom: 0;
    height: 14px;
    margin: auto;
    font-size: 14px;
    cursor: pointer;
    color: #ccc;
}
    </style>
                <div class="col-xs-3" style="float:left;margin:20px;margin-left:0;">
                 <input type="text" class="form-control input-lg" id="searchFilter" placeholder="بحث بالكود" />
                 <span id="searchclear" class="glyphicon glyphicon-remove-circle"></span>
                 </div>
                    <div class="panel-heading">
                     السادة الزوار لـ {{$event->title}} 

                    </div>

                

                    <div class="panel-body">
                        <table class="table table-striped task-table" id="mainTable">
                            <thead>
                                <th>الكود</th>
                                <th>الاسم</th>
                                <th>الجنس</th>
                                <th>الايميل</th>
                                <th>الهاتف</th>
                                <th>العنوان</th>
                                <th>رقم السيارة</th>
                                <th>ارسل</th>
                                
                            </thead>
                            <tbody>
                                @foreach ($guests as $guest)
                                    <tr class="tableRow">
                                        <td class="table-text code"><div>{{ $guest->id }}</div></td>
                                        <td class="table-text"><div>{{ $guest->name }}</div></td>
                                        <td class="table-text"><div>
                                        @if(!is_null($guest->isMale)&&$guest->isMale)
                                        ذكر
                                        @elseif(!is_null($guest->isMale)&&!$guest->isMale)
                                        انثي
                                        @endif
                                        </div></td>
                                        <td class="table-text"><div>{{ $guest->email }}</div></td>
                                        <td class="table-text"><div>{{ $guest->phone }}</div></td>
                                        <td class="table-text"><div>{{ $guest->address }}</div></td>
                                        <td class="table-text"><div>
                                        @if(!is_null($guest->car)&&$guest->car!='')
                                        {{ $guest->car }}
                                        @else
                                        لا توجد
                                        @endif
                                        </div></td>
                                        <td><a title="ارسال اشعار" href="{{ url('/pushNotifications/create?to='.$guest->id) }}" class="btn btn-danger"><i class="fa fa-bell fa-2x"></i></a>
                                      </td>
                                       
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <script>

                $(document).ready(function() {
                   
                    jQuery.expr[":"].Contains = jQuery.expr.createPseudo(function(arg) {
                        return function( elem ) {
                            return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
                        };
                    });

                    $("#searchclear").click(function(){
                         $("#searchFilter").val('').change();
                        });

                    $("#searchFilter").bind('change keyup',function () {
                        var unicode = event.charCode ? event.charCode : event.keyCode;
                        if (unicode == 27) { $(this).val(""); }
                        var data = this.value.split(" ");
                        var tbl = $("#mainTable ").find(".tableRow");
                        if (this.value == "") {
                            tbl.show();
                            return;
                        }
                        tbl.hide();
                        tbl.filter(function (i, v) {
                            var t = $(this).find(".code");
                         
                            for (var d = 0; d < data.length; ++d) {
                                // if (t.is(":Contains('" + data[d] + "')")) {
                                //     return true;
                                // }

                                if(t.text()==data[d]){
                                    return true;
                                }
                            }
                            return false;
                        }).show();
                    });



                });
                    </script>
                </div>
            @endif
            @stop