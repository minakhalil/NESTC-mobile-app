<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>NESTC control panel</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link rel="stylesheet" href="//cdn.rawgit.com/morteza/bootstrap-rtl/v3.3.4/dist/css/bootstrap-rtl.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">

<style>
body{
    font-family: 'Cairo', sans-serif;
}
.chip{
    display: inline-block;
    padding: 5px 20px;
    font-size: 16px;
    line-height: 30px;
    border-radius: 25px;
    
    color:#fff;
}
.bg-success{
    background-color:#5cb85c;
}
.bg-danger{
    background-color:#d9534f;
}
.btn-circle {
  width: 50px;
  /*height: 30px;*/
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.42;
  border-radius: 15px;
}
.navbar .navbar-nav {
  display: inline-block;
  float: none;
  vertical-align: top;
}

.navbar .navbar-collapse {
  text-align: center;
}
</style>
</head>
<body>

<nav class="navbar navbar-default center-item">
  <div class="container-fluid center-item">
    <div class="navbar-header ">
      <a class="navbar-brand center-item" href="{{ url('/') }}">NESTC Admin</a>
    </div>
    <div class="nav navbar-nav navbar-right ">
        <li><a href="{{ url('/events/') }}">الاحداث</a></li>
        <li><a href="{{ url('/pushNotifications/') }}">الاشعارات</a></li>
     
        
    </div>
      <div class="nav navbar-nav navbar-left">
        <li>
            <a style="float:left;" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                تسجيل الخروج
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
        
    </div>
  </div>
</nav>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<main>
    <div class="container">
        @yield('content')
    </div>
</main>

</body>
</html>