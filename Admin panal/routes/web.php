<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return redirect()->route('events.index');
});


Route::group(['middleware' => 'auth'], function () {


Route::resource('events', 'EventController');
// Route::resource('info', 'InfoController');

Route::get('/events/{event}/guests','EventController@getGuestsList');
Route::get('/events/{event}/geoLocations','EventController@getGeoLocations');

Route::resource('geoLocations', 'GeoLocationController');
Route::resource('pushNotifications', 'PushNotificationController');
Route::resource('registeredGuests', 'RegisteredGuestController');

Route::get('/info','InfoController@getInfo');
Route::get('/allRequest','MultipleEntitiesController@getCompleteData');




});