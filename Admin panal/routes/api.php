<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//sendScheduleNotification

Route::group(['middlewareGroups' => ['api'],'middleware' => ['cors']], function () {

Route::get('/notifications/user/{user}','PushNotificationController@getPush'); // get all
Route::get('/notifications/public','PushNotificationController@getPush'); // get only public

Route::get('/event/{event}/locations','EventController@getUpdatedLocations'); 
Route::get('/event/getintro','EventController@getEventForIntro');
Route::post('/near','MultipleEntitiesController@getCompleteData');
Route::post('/guest','RegisteredGuestController@store');


});