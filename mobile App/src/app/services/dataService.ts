import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

     
    //const BASE_URL = "http://192.168.1.14:8000/api/v1";
    const BASE_URL = "http://www.api.nestc-eg.com/api/v1";


@Injectable()
export class DataService {
        http: any;


    constructor (http:Http) {
        this.http = http;
    }



     index(lng,lat,uid) {
         let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
          let options = new RequestOptions({ headers: headers });

        return this.http.post(BASE_URL+'/near', {lng:lng,lat:lat,userId:uid}, options)
            .map(res => res.json())
            .catch(this.handleError);
             }

       getNotifications(id='') {
           var ii = (id!='')?'user/'+id:'public';
        return this.http.get(BASE_URL+'/notifications/'+ii)
            .map(res => res.json())
            .catch(this.handleError);
            }

        getForIntro() {
        return this.http.get(BASE_URL+'/event/getintro')
            .map(res => res.json())
            .catch(this.handleError);
            }
    
       refreshMap(id) {
        return Observable.interval(10000).flatMap(()=>this.http.get(BASE_URL+'/event/'+id+'/locations')
            .map(res => res.json())
            .retryWhen(error => error.delay(500))
            .catch(this.handleError));
    }
    

      sendUserData(user)
        {

    let headers = new  Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new  RequestOptions({ headers: headers });
 
        return this.http.post(BASE_URL+'/guest', user, options)
                  .map(res => res.json())
                  .catch(this.handleError);
            }


    handleError(error) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}
