import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { OneSignal } from '@ionic-native/onesignal';
import { HomePage } from '../pages/home/home';
import { WhoPage } from '../pages/who/who';
import { HelpPage } from '../pages/help/help';
import { InstPage } from '../pages/inst/inst';
import { StartPage } from '../pages/start/start';
import { NotificationPage } from '../pages/notification/notification';
import { FormPage } from '../pages/form/form';
import { Storage } from '@ionic/storage';

import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  templateUrl: 'app.html',
  animations: [
    trigger('menuIntro', [
      state('startAn', style({
        marginBottom: '0',
        transform: 'scale(1)',
        backgroundColor: '#2b0000'
      })),
      state('doneAn', style({
        marginBottom: '-56px',
        transform: 'scale(0.5)',
        backgroundColor: 'rgba(25, 25, 25, 0)'
      })),
      transition('*=>doneAn', animate('0.7s 1s')),
      transition('doneAn=>*', animate('0.1s'))
    ])
  ]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  data: any;
  newNotification: Boolean;
  pages: Array<{ title: string, component: any }>;
  dummy: any;
  loader: any;
  currentInv: any;
  logoAnimationState: any;
  userCode:any ="#";
  constructor(public platform: Platform, public statusBar: StatusBar, public loadingCtrl: LoadingController, public splashScreen: SplashScreen, private oneSignal: OneSignal, public events: Events, public storage: Storage) {
    this.initializeApp();
    this.data = {};
    this.newNotification = false;
    this.dummy = null;
    
    this.pages = [
      { title: 'تعليمات', component: InstPage },
      { title: 'مساعدة', component: HelpPage },
      { title: 'من نحن', component: WhoPage },
      { title: 'اخر الاخبار', component: NotificationPage }
    ];

  }

  initializeApp() {



    this.platform.ready().then(() => {

      this.splashScreen.hide();
      this.presentLoading();
      this.statusBar.styleDefault();

       // this.storage.clear().then(()=>{



      this.storage.get('userData').then((user) => {

        if (user && user.id) {
          
         
          this.rootPage = StartPage;
          
        } else {
          this.rootPage = FormPage;
        }

        this.loader.dismiss();

      });



       this.oneSignal.startInit('7303a016-ad64-4cdc-ae18-0476618ee168', '656374551160');

      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

      this.oneSignal.handleNotificationReceived().subscribe((data) => {

      this.events.publish('pushNotification:new', true);

      });

      this.oneSignal.handleNotificationOpened().subscribe(() => {
        this.storage.get('currentEventID').then((current) => {
        if(this.nav.getActive().component.name!="NotificationPage")
        if(current)
        this.nav.push(NotificationPage);
        else
        this.nav.setRoot(NotificationPage);
      });
      });
      this.oneSignal.endInit();

      
       });



      ////

     

      ////



  //  });
  }


  presentLoading() {

    this.loader = this.loadingCtrl.create({
      content: "جاري التحميل"
    });

    this.loader.present();

  }
  ngAfterViewInit() {
    this.logoAnimationState = 'startAn';
    this.events.subscribe('data:recieved', (data) => {
      this.data = data;
      this.storage.get('userData').then((user) => {

        if (user && user.id) {
          
          this.userCode = user.id+" # ";
        }
      });
    });
    this.events.subscribe('pushNotification:new', (data) => {
      this.newNotification = data;
    });
    
    //    this.events.subscribe('invitationCode:update', (data) => {   
    //       this.storage.get('currentInvitation').then((current) => {
    //         this.currentInv = current;
    //         this.events.unsubscribe('invitationCode:update');
    // });
    // });


  }


  openPage(page) {

    this.nav.push(page.component, this.data);
  }


  menuClosed() {
    this.logoAnimationState = 'startAn';
   // this.navStart();
  // console.log(this.logoAnimationState);
    this.events.publish('menu:closed', '');


  }

  movMapToEvent() {
    this.events.publish('map:event', '');
  }

  menuOpened() {
    this.logoAnimationState = 'doneAn';

    this.events.publish('menu:opened', '');

  }

  navEvent() {
    this.nav.setRoot(HomePage);
  }

  navUserInfo() {
  
  }
}
