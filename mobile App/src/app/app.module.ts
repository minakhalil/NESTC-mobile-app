import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { WhoPage } from '../pages/who/who';
import { HelpPage } from '../pages/help/help';
import { InstPage } from '../pages/inst/inst';
import { NotificationPage } from '../pages/notification/notification';
import { FormPage } from '../pages/form/form';

import { StartPage } from '../pages/start/start';
import { ErrorPage } from '../pages/error/error';
import { ModalPage } from '../pages/modal/modal';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from '@ionic-native/geolocation';
import {GoogleMaps} from '@ionic-native/google-maps';
import {OneSignal} from '@ionic-native/onesignal';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { PhotoViewer } from '@ionic-native/photo-viewer';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {DataService} from './services/dataService';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    WhoPage,
    HelpPage,
    InstPage,
    StartPage,
    ErrorPage,
    ModalPage,
    NotificationPage,
    FormPage
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__nestc',
         driverOrder: ['sqlite', 'indexeddb', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    WhoPage,
    HelpPage,
    InstPage,
    StartPage,
    ErrorPage,
    ModalPage,
    NotificationPage,
    FormPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    GoogleMaps,
    DataService,
    OneSignal,
    OpenNativeSettings,
    PhotoViewer,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
