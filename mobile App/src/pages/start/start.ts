import { Component ,ViewChild} from '@angular/core';
import { NavController, NavParams,MenuController ,Events,ToastController,Slides} from 'ionic-angular';
import { HomePage } from '../home/home';
import { ErrorPage } from '../error/error';
import { Geolocation } from '@ionic-native/geolocation';
import { OneSignal } from '@ionic-native/onesignal';

import { NotificationPage } from '../notification/notification';
import {DataService} from  '../../app/services/dataService';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';

import { Storage } from '@ionic/storage';
import { trigger, state, style, transition, animate } from '@angular/animations';
@Component({
  selector: 'page-start',
  templateUrl: 'start.html',
  animations: [
    trigger(
      'enterAnimationToTop', [
        transition(':enter', [
          style({transform: 'translateY(20%)', opacity: 0}),
          animate('400ms 900ms', style({transform: 'translateY(0)', opacity: 1}))
        ])
      ]
    )
    ,
    trigger(
      'enterAnimationFade', [
        transition(':enter', [
          style({ opacity: 0}),
          animate('200ms', style({opacity: 1}))
        ]
        ),
        transition(':leave', [
          style({ opacity: 1}),
          animate('200ms', style({opacity: 0}))
        ]
        )
      ]
    )
  ]
})
export class StartPage {
 isLoading:Boolean;
 error:{code:number,message:string}
 passedData:any;
 width:any;
introLoaded:Boolean=false;
introData:any=null;
slideOptions:any;
@ViewChild(Slides) slider: Slides;
  constructor(public navCtrl: NavController,public toastCtrl:ToastController,private openNativeSettings: OpenNativeSettings, public navParams: NavParams,public events: Events,private geolocation: Geolocation,public dataService:DataService,private oneSignal: OneSignal, private menu: MenuController, public storage: Storage) {
    this.isLoading = false;
    this.error = {code:null,message:null};
    this.passedData = navParams.get("passedData");
    this.slideOptions = {
      initialSlide: 0,
      pager: true
  };
    // if(this.passedData=="retry")
    setTimeout(()=>{
      this._getIntro();  
     
  },2500);
    ;
  }

    ionViewDidEnter() {
    this.menu.swipeEnable(false, 'menu1');
    this.width=(window.innerWidth);
  
    }

_getIntro(){

  this.dataService.getForIntro().subscribe(
            data => {
                    this.introData = data[0];                   
                    this.introLoaded=true;
                    
                   
                    },
            err => {
              
                let toast = this.toastCtrl.create({
                  message: 'هناك مشكلة بالاتصال بالانترنت',
                  duration: 3000,
                  position: 'bottom'
              
                });


                toast.present();
                this.introLoaded=true;
                    
                  });

}
  _handleStart(){

    this.isLoading = true;
    let  options = { maximumAge: 3000, timeout: 30000, enableHighAccuracy: true };

this.storage.get('userData').then((user) => {
this.geolocation.getCurrentPosition(options).then((resp) => {
        const {latitude ,longitude} = resp.coords;
        this.dataService.index(longitude,latitude,user.id).subscribe(
            data => {
                      if(data.data.event){
                        
                      this.oneSignal.sendTag("event"+data.data.event.id,"true");
                      this.storage.set('currentEventID', data.data.event.id).then(()=>{

                      

                      // if(data.data.invitation){
                      // this.storage.set('currentInvitation', data.data.invitation);
                      
                      
                      // }
                      this.events.publish('data:recieved', {event :data.data.event,static:data.data.info });
                      ///
                      this.events.publish('markers:upated', {data :data.data.event.geo_locations });

                      this.navHome(resp.coords,data);
                      //this.isLoading=false;
                      });
                    }
                    else{
                    
                    this.error.code = 3;
                    this.error.message = "عفوا، لا توجد احداث قريبة منك";
                    this._navToError(this.error);
                    this.storage.remove('currentEventID');
                  //  this.isLoading=false;

                    }
                   

                    },
            err => {               
                    
                    this.error.code = 0;
                    this.error.message = "تعذر الاتصال بالانترنت";
                    this.storage.remove('currentEventID');
                    this._navToError(this.error);
                   // this.isLoading=false;
                    });

      }).catch((error) => {

                              this.error.code = 1;
                              this.error.message = "تعذر الاتصال بخدمة الـGPS";
                              this.storage.remove('currentEventID');
                              this.openNativeSettings.open("location").then(()=>this._navToError(this.error));
                      });
});

  }




      // 1- network problem
      // 2- GPS problem
      // 3- cannot find nearplaces 
    _navToError(err) {
    this.navCtrl.push(ErrorPage,{passedData:err});
    }

  navHome(location,response) {
    this.navCtrl.setRoot(HomePage,{location :location,response:response });
  }

    navError() {
    this.navCtrl.setRoot(ErrorPage);
  }

}
