import { Component } from '@angular/core';
import { NavController, NavParams,MenuController } from 'ionic-angular';
import { StartPage } from '../start/start';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'page-error',
  templateUrl: 'error.html',
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateY(20%)', opacity: 0}),
          animate('400ms 900ms', style({transform: 'translateY(0)', opacity: 1}))
        ])
      ]
    )
  ]
})
export class ErrorPage {
 passedData:any;
 
  constructor(public navCtrl: NavController, public navParams: NavParams,private menu: MenuController) {
    this.passedData = navParams.get("passedData");
  }

    ionViewDidEnter() {
    this.menu.swipeEnable(false, 'menu1');
    }

      backToHome() {
      this.navCtrl.setRoot(StartPage,{passedData:"retry"});
    }


}
