import { Component } from '@angular/core';
import { NavController, NavParams, MenuController,Events, ToastController } from 'ionic-angular';
import {DataService} from  '../../app/services/dataService';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html'
})
export class NotificationPage {
isLoading:boolean;
notifications:any[];

  constructor(public navCtrl: NavController, public navParams: NavParams,public dataService:DataService, private menu: MenuController,public events: Events,public toastCtrl: ToastController, public storage: Storage) {
    //this.isLoading = true;
    this.notifications=[];
    
  }
   ionViewDidEnter() {
     this.loadNotifications();
     this.menu.close('menu1');
    this.menu.swipeEnable(false, 'menu1');
  }
  
    ngAfterViewInit() {
      
  this.events.subscribe('pushNotification:new', (data) => {
       if(data){
         this.loadNotifications();
       }
      });

  }

  loadNotifications(){
        this.isLoading = true;

      this.storage.get('userData').then((user) => {
      var ii = (user)?user.id:'';
     
        this.dataService.getNotifications(ii).subscribe(
            data => {
                    this.notifications = data;                   
                    this.isLoading=false;
                   
                    },
            err => {
              
                let toast = this.toastCtrl.create({
                  message: 'هناك مشكلة بالاتصال بالانترنت',
                  duration: 3000,
                  position: 'bottom'
              
                });


                toast.present();
              

                    this.isLoading=false;
                    
                  });
                  
              }).catch(()=>{
                this.notifications=[];
                this.isLoading=false;
              });
  }
 
}
