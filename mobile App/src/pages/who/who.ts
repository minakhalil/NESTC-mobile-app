import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';

@Component({
  selector: 'page-who',
  templateUrl: 'who.html'
})
export class WhoPage {
 who:any
  constructor(public navCtrl: NavController, public navParams: NavParams, private menu: MenuController) {
  this.who=this.navParams.get('static');
  }

    ionViewDidEnter() {
    this.menu.swipeEnable(false, 'menu1');
    }

}
