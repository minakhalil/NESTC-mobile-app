import { Component } from '@angular/core';
import { NavController,NavParams,Events,MenuController, ModalController,ToastController } from 'ionic-angular';
import {DataService} from  '../../app/services/dataService';
import { Storage } from '@ionic/storage';
import {
 GoogleMaps,
 GoogleMap,
 GoogleMapsEvent,
 LatLng,
 CameraPosition,
 AnimateCameraOptions,
 MarkerOptions,
 Marker,
 Polygon

} from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { ModalPage } from '../modal/modal';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
mapEv:GoogleMap;
location:any;
response:any;
loadingMyPosition:Boolean;
isModalOpened:Boolean;
mapMarkers:Marker[];
clickedMark:any;
  constructor(public navCtrl: NavController,public navParams: NavParams,public toastCtrl: ToastController,public dataService:DataService,private googleMaps: GoogleMaps,public events: Events,private geolocation: Geolocation, private menu: MenuController,public modalCtrl: ModalController, public storage: Storage) {
    this.location = navParams.get("location");
    this.response = navParams.get("response").data;
    this.loadingMyPosition = false;
    this.isModalOpened = false;
    this.mapMarkers=[];
    this.refreshMapElements();
  }
  ngAfterViewInit() {
    
    this.loadMap();
    this.events.subscribe('menu:opened', () => {
      this.mapEv.setClickable(false);
    });

    this.events.subscribe('menu:closed', () => {
      this.mapEv.setClickable(true);
    });

    this.events.subscribe('map:event', () => {
      this._goToEvent();
    });


  }

  ionViewDidEnter() {
    this.menu.swipeEnable(true, 'menu1');
    this.menu.open();
  }

  _openModal(data) {
   this.isModalOpened = true;
   let Modal = this.modalCtrl.create(ModalPage, { passed: data });
   Modal.present();
    Modal.onDidDismiss(() => {
    this.isModalOpened = false;
    this.menu.swipeEnable(true, 'menu1');
  });
}

  _goToEvent(){
 
     let position: AnimateCameraOptions = {
                      target: new LatLng(this.response.event.centerLat, this.response.event.centerLng),
                      zoom: this.response.event.zoomLevel,
                      tilt: 30,
                      bearing: 0
                    };


                this.mapEv.animateCamera(position);
  }
  _goToMyLocation() {
    this.loadingMyPosition = true;
     let  options = { maximumAge: 3000, timeout: 30000,enableHighAccuracy: true };
        this.geolocation.getCurrentPosition(options).then((resp) => {
                     this.location = resp.coords;
                     let position: AnimateCameraOptions = {
                      target: new LatLng(resp.coords.latitude, resp.coords.longitude),
                      zoom: 18,
                      tilt: 30,
                      bearing: 0
                    };


                this.mapEv.animateCamera(position);
                this.loadingMyPosition = false;
                        }).catch((error) => {
                          this.loadingMyPosition = false;
                           let toast = this.toastCtrl.create({
                  message: 'هناك مشكلة بالاتصال بالـGPS',
                  duration: 3000,
                  position: 'bottom'
                });


                toast.present();
                    
                      });


   
  }
  refreshMapElements(){
    this.storage.get('currentEventID').then((id) => {
     this.dataService.refreshMap(id).subscribe(
            data => {
                   this.response.event.geo_locations=data;
                   ///HERE publish event
                   //this.events.publish('markers:upated', {data :data });

                   this.response.event.geo_locations.map((element,i)=>{
                                var state=null;

                    var parking = (JSON.parse(this.response.event.geo_locations[i].jsonDetails).currentCapacity)?" ( "+JSON.parse(this.response.event.geo_locations[i].jsonDetails).currentCapacity+"/"+JSON.parse(this.response.event.geo_locations[i].jsonDetails).totalCapacity+" )":"";
                    

                    

                    if(JSON.parse(this.response.event.geo_locations[i].jsonDetails).currentCapacity){
                      state=parking;
                    }else{
                      if(JSON.parse(this.response.event.geo_locations[i].jsonDetails).emergency){
                        
                        state="طوارئ";
                      }
                      else{
                        if(JSON.parse(this.response.event.geo_locations[i].jsonDetails).isOpen){
                        
                          state=(JSON.parse(this.response.event.geo_locations[i].jsonDetails).isOpen==1)?"مفتوحة الان":"مغلقة الان";
                        }
                      }
                    }
                              
                              
                  //   if(element.type==0){
                    this.mapMarkers[i].setTitle(element.title);
                    
                    if(state)this.mapMarkers[i].setSnippet(state);
                    
                    if(this.mapMarkers[i].getHashCode()==this.clickedMark){
                      this.mapMarkers[i].showInfoWindow();
                    }
                  //   }
                   })
                 
                    },
            err => {
              
               let toast = this.toastCtrl.create({
                  message: 'هناك مشكلة بالاتصال بالانترنت',
                  duration: 3000,
                  position: 'bottom'
                });


                toast.present();
              
                  });

                  });
  }


  loadMap() {

    let element: HTMLElement = document.getElementById('map');
    let map: GoogleMap = this.googleMaps.create(element);
    
    map.one(GoogleMapsEvent.MAP_READY).then(
      () => {
        
        map.setMapTypeId("MAP_TYPE_SATELLITE");
        map.setMyLocationEnabled(true);
        this.response.event.geo_locations.map((locationElement,i)=>{

           var state=null;

           var parking = (JSON.parse(this.response.event.geo_locations[i].jsonDetails).currentCapacity)?" ( "+JSON.parse(this.response.event.geo_locations[i].jsonDetails).currentCapacity+"/"+JSON.parse(this.response.event.geo_locations[i].jsonDetails).totalCapacity+" )":"";
           

           var imgUrl;

           if(JSON.parse(this.response.event.geo_locations[i].jsonDetails).currentCapacity){
             imgUrl="./assets/icon/parking.png";
             state=parking;
           }else{
             if(JSON.parse(this.response.event.geo_locations[i].jsonDetails).emergency){
               imgUrl="./assets/icon/emergency.png";
               state="طوارئ";
             }
             else{
               if(JSON.parse(this.response.event.geo_locations[i].jsonDetails).isOpen){
                 imgUrl="./assets/icon/gate.png";
                 state=(JSON.parse(this.response.event.geo_locations[i].jsonDetails).isOpen==1)?"مفتوحة الان":"مغلقة الان";
                 
               }
               else{
                 imgUrl="./assets/icon/regular.png";

               }
             }
           }





        //  if(this.response.event.geo_locations[i].type==0) // marker
         // {
           
        map.addMarker({
          position: new LatLng(this.response.event.geo_locations[i].centerLat, this.response.event.geo_locations[i].centerLng),
          title: this.response.event.geo_locations[i].title ,
           
            icon: {
                url: imgUrl     ,
                size: {
              width: 39,
              height: 51
            } 
              },
            styles : {
              'text-align': 'center',
              'font-weight': 'bold',
             
            }
           }).then((marker: Marker) => {
             this.mapMarkers.push(marker);
             this.response.event.geo_locations[i].marker = marker;
             if(state)
             marker.setSnippet(state);
            !this.isModalOpened&&marker.on(GoogleMapsEvent.INFO_CLICK).subscribe(()=>{
                this.response.event.geo_locations[i].id=i;
              this._openModal({item:this.response.event.geo_locations[i]});
             
            });

            marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(()=>{
              this.clickedMark = marker.getHashCode(); 
            })

          });





        //}
        
       
          if(this.response.event.geo_locations[i].type==1) {
           
          map.addPolygon({
            'points': JSON.parse(this.response.event.geo_locations[i].jsonDetails).points,
            'fillColor': this.response.event.geo_locations[i].style,
            'strokeWidth': 1,
            'strokeColor' : 'rgba(215, 44, 44, 0)'

          }).then((polygon: Polygon) => {
          
              !this.isModalOpened&&polygon.on(GoogleMapsEvent.OVERLAY_CLICK).subscribe(() => {
              this.response.event.geo_locations[i].id=i;
              this._openModal({item:this.response.event.geo_locations[i]});
              })

          });

      
          }

        

        });
        
        
            let position: CameraPosition = {
      target: new LatLng(this.location.latitude, this.location.longitude),
      zoom: 18,
      tilt: 30
    };


    map.moveCamera(position);

    this.mapEv = map;


      }
    );



  }






}
