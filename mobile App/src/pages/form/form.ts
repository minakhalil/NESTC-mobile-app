import { Component } from '@angular/core';
import { NavController, NavParams, MenuController,Events, ToastController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {DataService} from  '../../app/services/dataService';
import { Storage } from '@ionic/storage';
import { StartPage } from '../start/start';
import { OneSignal } from '@ionic-native/onesignal';
@Component({
  selector: 'page-form',
  templateUrl: 'form.html'
})


export class FormPage {
isLoading:boolean;
introForm : FormGroup;

  constructor(public navCtrl: NavController,private oneSignal: OneSignal, public navParams: NavParams,public dataService:DataService, private menu: MenuController,public events: Events,public toastCtrl: ToastController,private formBuilder: FormBuilder, public storage: Storage) {
    this.isLoading = false;

    this.introForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required,Validators.email])],
      phone: ['', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
      gender: ['ذكر', Validators.required],
      car: [''],
    });
  

  }
   ionViewDidEnter() {
    this.menu.swipeEnable(false, 'menu1');
  }
  
   formSubmit(){
    
    this.isLoading=true;
    this.introForm.value.isMale = this.introForm.value.gender=="ذكر"?true:false;
    delete this.introForm.value.gender;
    this.oneSignal.getIds().then((ids)=>{

    this.introForm.value.tracking_id = ids.userId;

    this.dataService.sendUserData(this.introForm.value).subscribe(
            data => {
                    this.storage.set('userData', data);
                    this.isLoading=false;
                    this.navCtrl.setRoot(StartPage);
                    },
            err => {


              
                let toast = this.toastCtrl.create({
                  message: 'هناك مشكلة بالاتصال بالانترنت',
                  duration: 3000,
                  position: 'bottom'
                });


                toast.present();
              

                    this.isLoading=false;
                    
                    });


    })
    
   

  }

     



 
}
