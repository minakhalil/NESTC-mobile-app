import { Component } from '@angular/core';
import { NavController, NavParams, MenuController,Events,ToastController,Platform } from 'ionic-angular';
import {DataService} from  '../../app/services/dataService';
import { Storage } from '@ionic/storage';
import { ISubscription } from "rxjs/Subscription";
//import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';

@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html'
})
export class ModalPage {
 data:any;
 subs:ISubscription
  constructor(public navCtrl: NavController,public events: Events,public navParams: NavParams, private menu: MenuController,public toastCtrl: ToastController,public dataService:DataService,public storage: Storage,private platform: Platform) {
  this.data = this.navParams.get("passed").item;
  this.data.desc=this.data.description;
  this.data.capacity = (JSON.parse(this.data.jsonDetails).currentCapacity)?" ( "+JSON.parse(this.data.jsonDetails).currentCapacity+"/"+JSON.parse(this.data.jsonDetails).totalCapacity+" )":null; 
  this.data.openState = (JSON.parse(this.data.jsonDetails).isOpen)?JSON.parse(this.data.jsonDetails).isOpen:null;
  this.data.emergency = (JSON.parse(this.data.jsonDetails).emergency)?JSON.parse(this.data.jsonDetails).emergency:null;
  this.data.indoor=JSON.parse(this.data.indoor);
  this.refreshData();

}

  // ngAfterViewInit() {
  //   this.events.subscribe('markers:upated', (data) => {
  //     if(this.data)
  //     this.data=data[this.data.id-1];
  //   });


  //}

    ionViewDidLeave(){
      
       this.subs.unsubscribe()
      this.events.publish('menu:closed', '');
    }
    refreshData(){
    this.storage.get('currentEventID').then((id) => {
    this.subs= this.dataService.refreshMap(id).subscribe(
            data => {
              if(this.data){
          this.data.title=data[this.data.id].title;
          this.data.desc=data[this.data.id].description;
          this.data.capacity = (JSON.parse(data[this.data.id].jsonDetails).currentCapacity)?" ( "+JSON.parse(data[this.data.id].jsonDetails).currentCapacity+"/"+JSON.parse(data[this.data.id].jsonDetails).totalCapacity+" )":null; 
          this.data.openState = (JSON.parse(data[this.data.id].jsonDetails).isOpen)?JSON.parse(data[this.data.id].jsonDetails).isOpen:null;
          this.data.emergency = (JSON.parse(data[this.data.id].jsonDetails).emergency)?JSON.parse(data[this.data.id].jsonDetails).emergency:null;
            }
                    },
            err => {
              
               let toast = this.toastCtrl.create({
                  message: 'هناك مشكلة بالاتصال بالانترنت',
                  duration: 3000,
                  position: 'bottom'
                });


                toast.present();
              
                  });

                  });
  }

    ionViewDidEnter() {
    this.menu.swipeEnable(false, 'menu1');
    this.events.publish('menu:opened', '');
    }
  
  closeModal() {
        this.navCtrl.pop();
    }

    launchNavigator(){
     // this.launchNavigator.navigate([this.data.centerLat,this.data.centerLng]);
     console.log("launch now ..");
     let destination = this.data.centerLat + ',' + this.data.centerLng;

      if(this.platform.is('ios')){
        window.open('maps://?q=' + destination, '_system');
      } else {
        let label = encodeURI(this.data.title);
        window.open('geo:0,0?q=' + destination + '(' + label + ')', '_system');
      }
    }

}
