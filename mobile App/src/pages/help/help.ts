import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';

@Component({
  selector: 'page-help',
  templateUrl: 'help.html'
})
export class HelpPage {
  event:any;
  jsonDone:any=false;
  constructor(public navCtrl: NavController, public navParams: NavParams, private menu: MenuController) {
  this.event=this.navParams.get('event');
  
  this.event.helpNumber=this.toJson(this.event.helpNumber);
  
  }
    ionViewDidEnter() {
    this.menu.swipeEnable(false, 'menu1');
    }

 toJson(str) {
   var jsonStr=str;
    try {
      jsonStr =  JSON.parse(str);
    } catch (e) {
      this.jsonDone=true;
        return str;
    }
      this.jsonDone=true;

    return jsonStr;
}

}
