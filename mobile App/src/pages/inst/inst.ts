import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import {SafeResourceUrl, DomSanitizer} from '@angular/platform-browser';
import { PhotoViewer } from '@ionic-native/photo-viewer';

@Component({
  selector: 'page-inst',
  templateUrl: 'inst.html'
})
export class InstPage {
 event:any;
 videoUrl: SafeResourceUrl;
  constructor(public navCtrl: NavController,private domSanitizer: DomSanitizer, public navParams: NavParams,private photoViewer: PhotoViewer, private menu: MenuController) {
  this.event=this.navParams.get('event');
  this.videoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.event.instVideo);


  }

    ionViewDidEnter() {
    this.menu.swipeEnable(false, 'menu1');
  }
  
  viewImage(){
  this.photoViewer.show(this.event.localMapUrl, this.event.title, {share: false});
  }


}
